package ca.claurendeau.domaine;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ANIMAL")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ANIMAL_TYPE")
public abstract class Animal {
    
    public static Dog createDog(String animalName) {
        return new Dog(animalName);
    }
    
    public static Cat createCat(String animalName) {
        return new Cat(animalName);
    }
    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    @Id
    private Long id = -1l;

    @Column(name="ANIMAL_NAME")
    protected String animalName;
    
    @ManyToOne
    protected AnimalOwner animalOwner;


}
